<?php
/****************************************************
 * library of ggerman
 * license GPL 2 attached
 * contact me ggerman@gmail.com
 * ZenCart to Merchant of google
 *
 * generation of feed by txt procedure and upload the file
 *
 * Product contain the class for get products of zencart and generate txt for google merchant
 * ***************************************************/

require_once 'products.php';

class GoogleFeed extends Product {
  public function render() {
    if($this->getStatus()) {
      $exclude = file_get_contents('exclude.txt');
      if(strpos($exclude, $this->id) === false) {
        echo ".";
      } else {
        echo "x";
        $this->gtin = null;
      }
      return "{$this->id}".TAB."{$this->title}".TAB."{$this->description}".TAB."{$this->google_product_category}".TAB."{$this->product_type}".TAB."{$this->link}".TAB."{$this->image_link}".TAB."{$this->condition}".TAB."{$this->availability}".TAB."{$this->price}".TAB."{$this->sale_price}".TAB."{$this->sale_price_effective_date}".TAB."{$this->gtin}".TAB."{$this->brand}".TAB."{$this->mpn}".TAB."{$this->item_group_id}".TAB."{$this->gender}".TAB."{$this->age_group}".TAB."{$this->color}".TAB."{$this->size}".TAB."{$this->shipping}".TAB."{$this->shipping_weigth}\n\n";
    }
  }
}

class GoogleFeeds extends Products {
  public function createFeed() {
    file_put_contents(FEED_FILENAME, HEADER);
    foreach($this->products as $index => $register) {
      file_put_contents(FEED_FILENAME, $register->render(), FILE_APPEND);
    }
  }
}
