<?php
/***********************
 * configure all constants with your information and rename the file to configure.php
 * *********************/

// Characters
define('TAB', "\t");
define('DOT', ".");

// Ftp google merchant connection
define('FTP_HOSTS', 'uploads.google.com');
define('FTP_USER', 'username');
define('FTP_PASSWORD', 'password');

// HEADER line for feed txt file
define('HEADER', "id	title	description	google product category	product type	link	image link	condition	availability	price	sale price	sale price effective date	gtin	brand	mpn	item group id	gender	age group	color	size	shipping	shipping weight\n");

// Environment
define('URL_WEBSITE', "url website");
define('URL_WEBSITE_IMAGES', "url web site image folder");

// database configuration
define('DB_SERVER', 'xxxxxxxxx');
define('DB_SERVER_USERNAME', 'user_name');
define('DB_SERVER_PASSWORD', 'password');
define('DB_DATABASE', 'database_name');

// Feed file name
define('FEED_FILENAME', 'feed.txt');

